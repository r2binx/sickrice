#! /bin/bash

IP=$(dig +short myip.opendns.com @resolver1.opendns.com)


if pgrep -x openvpn > /dev/null; then
    echo -e "%{F#4CAF50} %{F#FFFFFF}$IP"
else
    echo -e "%{F#d50000} %{F#FFFFFF}$IP"
fi
