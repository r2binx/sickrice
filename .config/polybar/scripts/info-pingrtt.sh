#!/bin/sh

HOST=8.8.8.8
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'

if ! ping=$(ping -n -c 1 -W 1 $HOST); then
    echo "%{F#d50000} ping failed"
else
    rtt=$(echo "$ping" | sed -rn 's/.*time=([0-9]{1,})\.?[0-9]{0,} ms.*/\1/p')

    if [ "$rtt" -lt 50 ]; then
	icon="%{F#4CAF50}"
    elif [ "$rtt" -lt 150 ]; then
    	icon="%{F#FFC107}"
    else
    	icon="%{F#d50000}"
    fi

    echo -e "$icon %{F#FFFFFF}$rtt ms"
fi
